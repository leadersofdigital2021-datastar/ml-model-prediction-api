#!flask/bin/python
import joblib
import numpy as np
import pandas as pd
import re
from flask import Flask, jsonify, abort
from flask import request
from flask import make_response
from scipy import sparse
from sklearn.metrics.pairwise import cosine_similarity

app = Flask(__name__)

model = None
sparse_matrix_okpd2 = None
okpd2 = None


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)


@app.route('/predict', methods=['POST'])
def predict_model():
    if not request.json and 'purchase_name' not in request.json:
        abort(400)

    names = request.json['purchase_name']
    result = [predict(model, sparse_matrix_okpd2, okpd2, '', name, 1)[0] for name in names]

    return jsonify({'okpd2_code': result}), 201


def predict(model, sparse_matrix, okpd2_df, okpd2_code, purchase_name, count_preds: int = 3) -> list:
    predictions = cosine_similarity(model.transform([purchase_name]), sparse_matrix).flatten()
    result = [] if not okpd2_code or re.match(r'[\d]{2}.[\d]{1,2}', str(okpd2_code)) is None else [okpd2_code]
    manufacturer_code = False

    while len(result) < count_preds:
        idx = np.argmax(predictions)
        if not manufacturer_code:
            idx = np.argmax(predictions * okpd2_df['manufacturer'].values)
            manufacturer_code = True
        predictions[idx] = -1
        result.append(okpd2_df.loc[idx, 'code'])

    return result


if __name__ == '__main__':
    with open('model.pkl', 'rb') as handle:
        okpd2 = pd.read_csv('okpd2_df.csv')
        model = joblib.load('model.pkl')
        sparse_matrix_okpd2 = sparse.load_npz('sparse_matrix_okpd2.npz')
    app.run(debug=False, host='0.0.0.0', port=5000)

